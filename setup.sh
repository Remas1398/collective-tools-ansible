#!/bin/bash

DOMAIN="$1"

IP="$(hcloud server create --name $DOMAIN --image debian-10 --type cx21 --location hel1 --ssh-key p-jo --ssh-key andreas  | tail -n 1 | sed -e 's#.*: \(\)#\1#')"
hcloud server enable-backup $DOMAIN

echo "New server with IP: ${IP}"

source api.env

curl -X POST -H "Content-Type: application/json" \
             -H "X-Api-Key: $KEY" \
             -d '{"rrset_ttl": 1800,
                  "rrset_values": ["'$IP'"]}' \
             https://dns.api.gandi.net/api/v5/zones/$ZONE/records/$DOMAIN/A

curl -X POST -H "Content-Type: application/json" \
             -H "X-Api-Key: $KEY" \
             -d '{"rrset_ttl": 1800,
                  "rrset_values": ["'$IP'"]}' \
             https://dns.api.gandi.net/api/v5/zones/$ZONE/records/*.$DOMAIN/A

echo "Wait for two minutes"

sleep 120

ansible-playbook install.yml -i inventory -l $DOMAIN.collective.tools
