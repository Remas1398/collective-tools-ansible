#!/bin/bash

DOMAIN="$1"

if [[ "$OSTYPE" == "linux-gnu" ]]; then
  IP="$(./../go/bin/hcloud server create --name $DOMAIN --image debian-10 --type cx21 --location hel1 --ssh-key p-jo --ssh-key pi | tail -n 1 | sed -e 's#.*: \(\)#\1#')"
  ./../go/bin/hcloud server enable-backup $DOMAIN
else
  IP="$(hcloud server create --name $DOMAIN --image debian-10 --type cx21 --location hel1 --ssh-key p-jo --ssh-key pi  | tail -n 1 | sed -e 's#.*: \(\)#\1#')"
  hcloud server enable-backup $DOMAIN
fi

echo "New server with IP: ${IP}"
