alias occ="/usr/bin/php /var/www/nextcloud/occ"
# https://docs.nextcloud.com/server/12/admin_manual/configuration_server/occ_command.html#enabling-autocompletion
source <(/usr/bin/php /var/www/nextcloud/occ _completion --generate-hook --program occ)

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

