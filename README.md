## Preparations

If you haven't got a hetzner server and DNS record, please go to [this page](https://gitlab.com/collective.tools/collective-tools-ansible/wikis/Server-creation-&-setup).

When you have got them, install this repository to a directory of your choice.

If you haven't got Ansible yet, download it [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html?extIdCarryOver=true&sc_cid=701f2000001OH7YAAW).

Then, open the terminal in that directory you chose and enter this Ansible command:

``ansible-galaxy install -r requirements.yml --force``

Now before you start the installation, create a folder in the directory called "inventory", and inside the folder, create a file called "static.yml".

The "static.yml" shall contain:

```bash
dev:
  hosts:
    development.example.com:
      subdomain: development
```

Then create a folder "group_vars" inside "inventory", and create a file called "all.yml"

"all.yml" shall contain:
```bash
distro: buster
main_domain: example.com
hostname: "{{ subdomain }}.{{ main_domain }}"

smtp_hostname:
smtp_port:
smtp_username:
smtp_passw:

admin_email:
admin_username:

title: Collective Tools

alert_settings:
   email_server:
     name: "{{ smtp_hostname }}"
     port: "{{ smtp_port }}"
     user: "{{ smtp_username }}"
     pw: "{{ smtp_passw }}"
   admin_email:
```


## Installation

Before installing, make sure you done everything in the previous step.

Enter this command in the terminal in the directory in which the repository is.

This command will install the server and configure all of the settings automatically:

```bash
ansible-playbook install.yml -i inventory -l <hostname>
```

During the installation a `admin` user account will be created and the random password for this user can be found in `/root/.nextcloud` on the server.

To log in to the server to access the admin password, type this in the terminal:

```ssh root@(hostname)```

---
