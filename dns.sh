#!/bin/bash

DOMAIN="$1"
IP="$2"

source api.env

curl -X POST -H "Content-Type: application/json" \
             -H "X-Api-Key: ${KEY}" \
             -d '{"rrset_ttl": 1800,
                  "rrset_values": ["'${IP}'"]}' \
             https://dns.api.gandi.net/api/v5/zones/${ZONE}/records/${DOMAIN}/A

curl -X POST -H "Content-Type: application/json" \
             -H "X-Api-Key: ${KEY}" \
             -d '{"rrset_ttl": 1800,
                  "rrset_values": ["'${IP}'"]}' \
             https://dns.api.gandi.net/api/v5/zones/${ZONE}/records/*.${DOMAIN}/A
